void main() {
  Question3 c = new Question3();
//  a)
  print(
      'The winner of the  ${c.Year} Mtn App of the Year  was    ${c.Name} Developed by ${c.Developer}  winning awards in the  ${c.Category} categories');

// b)
  c.questionB();
}

class Question3 {
  var Year = "2021";
  var Name = "Ambani Africa";
  var Category =
      "Best Gaming Solution, Best Educational and Best South African Solution";
  var Developer = "Mukundi Lambani";

  questionB() {
    print(
        'The winner of the  $Year Mtn App of the Year  was    ${Name.toString().toUpperCase()} Developed by $Developer  winning awards in the  $Category categories');
  }
}
