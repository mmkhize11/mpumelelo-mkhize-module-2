// Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; a) Sort and print the apps by name;  b) Print the winning app of 2017 and the winning app of 2018.; c) the Print total number of apps from the array.
void main() {
  List<Map<dynamic, dynamic>> appOfTheYearWinners = [
    {"Year": 2012, "Name": "Fnb-banking"},
    {"Year": 2013, "Name": "SnapScan"},
    {"Year": 2014, "Name": "Live Inspect"},
    {"Year": 2015, "Name": "WumDrop"},
    {"Year": 2016, "Name": "Domestly"},
    {"Year": 2017, "Name": "Standard bank Shyft"},
    {"Year": 2018, "Name": "Khula"},
    {"Year": 2019, "Name": "Naked"},
    {"Year": 2020, "Name": "Easy Equities"},
    {"Year": 2021, "Name": "Ambani"},
  ];

  // a)
  appOfTheYearWinners.sort((a, b) => Comparable.compare(a['Name'], b['Name']));

  appOfTheYearWinners.forEach((element) {
    print('${element['Name']}');
  });

  // b)
  var winner2017 = appOfTheYearWinners.where((c) => c['Year'] == 2017).map((e) {
    return e['Name'];
  });
  var winner2018 = appOfTheYearWinners.where((c) => c['Year'] == 2018).map((e) {
    return e['Name'];
  });

  print(
      'app winner in  2017 was ${winner2017} and the app winner in 2018 was ${winner2018}');
  // c)
  print('Number of winners so far   ${appOfTheYearWinners.length}');
}
