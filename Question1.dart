// importing dart:io file
import 'dart:io';

// Write a basic program that stores and then prints the following data: Your name, favorite app, and city;
void main() {
  print("Enter your name?");
  // Gets your name
  String? name = stdin.readLineSync();
  // print('Hello , my name is  $name ');
  // print('Hello , my name is  $name ');
  // Gets Favourite app
  print("Enter your Favourite App?");
  String? FavApp = stdin.readLineSync();
  // print('My favourite app is  $FavApp ');
  // print('My favourite app is  $FavApp ');
// Gets City
  print("Enter your City?");
  String? City = stdin.readLineSync();
  // print('My City is  $City ');

  print(
      'Hello , my name is  $name    , My favourite app is ${FavApp}  and i am from $City');

  // print("Hello, $name! \nWelcome to GeeksforGeeks!!");
}
